# sdl

Voici un squelette de programme SDL2.
Les variables principales sont déclarées comme globales.

Les initialisations graphiques sont faites : la bibliothèque principale et les bibliothèques TTF et IMAGE.
Quelques ressources sont chargées mais non utilisées : deux polices de caractères et un logo.

Le programme propose également une boucle d'événements "minimaliste" avec un affiche de fenêtre au contenu simple.

## compilation et exécution

Si la bibliothèque SDL2 est correctement installées, il suffit de "compiler" avec les options
    -lSDL2 -lSDL2_image -lSDL2_ttf 
(c'est le cas pour ada et les machines virtuelles à l'ISIMA)
Ces options ne sont utiles que pour l'édition des liens

Pour vos installations perso, il faudra potentiellement trouver et indiquer 
où se trouvent les fichiers d'entête avec l'option -I
(par exemple -I/opt/local/include)
et les fichiers des bibliothèques avec l'option -L
(par exemple -L/opt/local/lib )
    

*/